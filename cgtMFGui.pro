QT += qml quick

CONFIG += c++11 qt plugin

SOURCES += main.cpp \
    runconfig/runconfig.cpp

RESOURCES += \
    cgtmfgui.qrc

# release target: global deactivation of qDebug() prints
CONFIG(release, debug|release):{
    # deactivating qDebug() prints (C++)
    DEFINES += QT_NO_DEBUG_OUTPUT
}

RC_ICONS = images/cgt_ico_notext.ico

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = ./runconfig

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    SettDialog.qml \
    AbDialog.qml \
    LicDialog.qml \
    main.qml \
    config_example/stdprod_conf.xml \
    runconfig/qmldir

HEADERS += \
    runconfig/runconfig.h
