/* ************************************************************************/ /**
 * @file    LicDialog.qml
 * *****************************************************************************
 * @brief   Defining licensing dialog content...
 *
 * *****************************************************************************
 * *****************************************************************************
 * @author      Alexander Pockes (ATP)   <alexander.pockes@congatec.com>
 *
 * @copyright   Copyright 2016 Alexander Pockes, congatec AG
 *
 * @license     cgtMFGui is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License version 3
 *              as published by the Free Software Foundation.
 *
 *              cgtMFGui is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the
 *              GNU General Public License along with this program.
 *              If not, see <http://www.gnu.org/licenses/>.
 * ************************************************************************/ /**
 **************************************************************************** */


import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls 1.2

import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

import Qt.labs.settings 1.0


Dialog {
    width: 600
    height: 250

    title: qsTr("Licensing Information...")

    ColumnLayout {
        Layout.fillWidth: true

        Label {
            text: "Licensing Information"
            font.pixelSize: 20
        }

        Label {
            text: ""
        }

        Label {
            text: "<a href='"
                  + mainWin.sourceRepoURL
                  + "'>cgtMFGui</a> itself is <a href='file:"
                  + applicationDirPath
                  + "/COPYING/gpl-3.0.txt'>GPLv3</a> licensed open source software."

            onLinkActivated: Qt.openUrlExternally(link)

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton
                cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
            }
        }

        Label {
            text: "Its source and/or its distribution is based on otherwise licensed open source software:"

            onLinkActivated: Qt.openUrlExternally(link)

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton
                cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
            }
        }

        Label {
            text: "- Qt SDK "
            + mainWin.qtVersion
            + ", <a href='file:"
            + applicationDirPath
            + "/COPYING/lgpl-3.0.txt'>LGPLv3</a>, "
            + " <a href='"
            + mainWin.qtWebsiteURL
            + "'>"
            + mainWin.qtWebsiteURL
            + "</a>"

            onLinkActivated: Qt.openUrlExternally(link)

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton
                cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
            }
        }

        Label {
            text: "- MinGW "
                  + mainWin.mingwVersion
                  + ", <a href='file:"
                  + applicationDirPath
                  + "/COPYING/MIT_License.txt'>MIT-License</a>, "
                  + "<a href='"
                  + mainWin.mingwWebsiteURL
                  + "'>"
                  + mainWin.mingwWebsiteURL
                  + "</a>"

            onLinkActivated: Qt.openUrlExternally(link)

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton
                cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
            }
        }

        Label {
            text: ""
        }

        Text {
            text:
                "The cgtMFGui (source) distribution contains unfree image material (congatec logo files).\n"
                + "You are not granted any trademark rights or licenses to the trademarks of the\n"
                + "congatec AG or any party, including without limitation to the congatec logo files."
        }
    }
}
