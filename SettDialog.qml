/* ************************************************************************/ /**
 * @file    SettDialog.qml
 * *****************************************************************************
 * @brief   Defining settings dialog content...
 *
 * *****************************************************************************
 * *****************************************************************************
 * @author      Alexander Pockes (ATP)   <alexander.pockes@congatec.com>
 *
 * @copyright   Copyright 2016 Alexander Pockes, congatec AG
 *
 * @license     cgtMFGui is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License version 3
 *              as published by the Free Software Foundation.
 *
 *              cgtMFGui is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the
 *              GNU General Public License along with this program.
 *              If not, see <http://www.gnu.org/licenses/>.
 * ************************************************************************/ /**
 **************************************************************************** */


import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls 1.2

import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

// XXX: QML Settings functionality is still in development...
import Qt.labs.settings 1.0


Dialog {
    property alias execPath: settDialogSettings.execPath
    property alias configFilePath: settDialogSettings.configFilePath

    width: 640
    height: 180

    title: qsTr("Settings")

    standardButtons: StandardButton.Save | StandardButton.Cancel

    ColumnLayout {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: spacing

        Layout.fillWidth: true

        GroupBox {
            title: qsTr("Pathes")
            Layout.fillWidth: true

            GridLayout {
                columns: 3
                rows: 1
                columnSpacing: 5

                anchors.fill: parent
                Layout.fillWidth: true

                Label {
                    text: qsTr("MFGTool2 Executable:")
                }
                TextField {
                    id: execPathTField
                    text: settDialogSettings.execPath

                    Layout.fillWidth: true
                }
                Button {
                    text: qsTr("find...")
                    onClicked: execPathDialog.open()
                }


                Label {
                    text: qsTr("cgtMFGui Config File:")
                }
                TextField {
                    id: configFilePathTField
                    text: settDialogSettings.configFilePath

                    Layout.fillWidth: true
                }
                Button {
                    text: qsTr("find...")
                    onClicked: confFilePathDialog.open()
                }
            }
        }

        FileDialog {
            id: execPathDialog

            title: qsTr("Select File...")

            onAccepted: {
                execPathTField.text = execPathDialog.fileUrl.toString().slice(8);
            }
        }

        FileDialog {
            id: confFilePathDialog

            title: qsTr("Select File...")

            onAccepted: {
                configFilePathTField.text = confFilePathDialog.fileUrl.toString().slice(8);
            }
        }

        Settings {
            id: settDialogSettings

            property string execPath: "../MFGTool2.exe"
            property string configFilePath: "../cgtMFGui_config/stdprod_conf.xml"
        }

    }

    onAccepted: {
        settDialogSettings.execPath = execPathTField.text;
        settDialogSettings.configFilePath = configFilePathTField.text;
    }
}
