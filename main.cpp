/* ************************************************************************/ /**
 * @file    main.cpp
 * *****************************************************************************
 * @brief   cgtMFGui - congatec Manufacturing user interface
 *
 *          cgtMFGui is a simple Qt-based GUI application supporting
 *          inexprienced users of the NXP MFGTool2.
 *          cgtMFGui trys to completely supersede manual editing of the
 *          NXP MFGTool configuration file (cfg.ini).
 *          cgtMFGui hides all configuration details from the user.
 *
 * *****************************************************************************
 * *****************************************************************************
 * @author      Alexander Pockes (ATP)   <alexander.pockes@congatec.com>
 *
 * @copyright   Copyright 2016 Alexander Pockes, congatec AG
 *
 * @license     cgtMFGui is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License version 3
 *              as published by the Free Software Foundation.
 *
 *              cgtMFGui is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the
 *              GNU General Public License along with this program.
 *              If not, see <http://www.gnu.org/licenses/>.
 * ************************************************************************/ /**
 **************************************************************************** */


#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QIcon>
#include <QQmlContext>

#include <QDebug>


#include "runconfig/runconfig.h"



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);


    // requried by Qt's settings mechanism, see SettDialog.qml
    // -> used to create the windows registry entries...
    app.setOrganizationName("congatec AG");
    app.setOrganizationDomain("congatec.com");
    app.setApplicationName("cgtMFGui");

    // setting window icon separately, because .ico file doesn't scale well there
    app.setWindowIcon(QIcon("qrc:/images/cgt_logo_notext.png"));

    // creating an runconfig instance (C++ QML extension plugin)
    RunConfig rc;
    // registering org.congatec.qt.qml.cgtMFGui as QML type/module
    rc.registerTypes("org.congatec.qt.qml.cgtMFGui");

    // loading main.qml into mainwindow
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("applicationDirPath", QGuiApplication::applicationDirPath());
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));


    // indicating startup due to some former deployment issues at windows plattform
    qInfo() << "INFO: startup..." << endl;

    return app.exec();
}
