/* ************************************************************************/ /**
 * @file    AbDialog.qml
 * *****************************************************************************
 * @brief   Defining about dialog content...
 *
 * *****************************************************************************
 * *****************************************************************************
 * @author      Alexander Pockes (ATP)   <alexander.pockes@congatec.com>
 *
 * @copyright   Copyright 2016 Alexander Pockes, congatec AG
 *
 * @license     cgtMFGui is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License version 3
 *              as published by the Free Software Foundation.
 *
 *              cgtMFGui is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the
 *              GNU General Public License along with this program.
 *              If not, see <http://www.gnu.org/licenses/>.
 * ************************************************************************/ /**
 **************************************************************************** */


import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls 1.2

import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

import Qt.labs.settings 1.0


Dialog {

    property string toolchainVersion: "Qt "
                                      + qtVersion
                                      + " (MinGW "
                                      + mingwVersion
                                      + ")"

    width: 600
    height: 250

    title: qsTr("About cgtMFGui...")


    RowLayout {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 5

        spacing: 20

        Layout.fillWidth: true

        Image {
            id: cgtStLogo

            Layout.preferredWidth: 200
            Layout.preferredHeight: 200

            Layout.minimumWidth: 200
            Layout.minimumHeight: 200

            fillMode: Image.PreserveAspectFit

            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            source: mainWin.cgtLogoCut
        }

        ColumnLayout {
            id: abDialogRightSide
            spacing: 10

            Layout.preferredWidth: 400
            Layout.minimumWidth: 400

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Label {
                text: "cgtMFGui " + mainWin.toolVersion
                font.pixelSize: 20
            }

//            Label {
//                text: "!! COMPANY INTERNAL USE ONLY !!"
//                font.pixelSize: 16
//            }

            Label {
                text: "Based on " + toolchainVersion
            }

            Label {
                text: "Copyright " + mainWin.copyrightInformation
            }

            Label {
                text: "<a href='"
                + mainWin.cgtWebsite
                + "'>"
                + mainWin.cgtWebsite
                + "<a>"

                onLinkActivated: Qt.openUrlExternally(link)

                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.NoButton
                    cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                }
            }

            Label {
                text: "License: GPLv3, <a href='License'>see further details</a>"

                onLinkActivated: {
                    licensingDialog.open();
                }

                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.NoButton
                    cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                }
            }

            Label {
                text: "Source Code: <a href='"
                + mainWin.sourceRepoURL
                + "'>"
                + mainWin.sourceRepoURL
                + "<a>"

                onLinkActivated: Qt.openUrlExternally(link)

                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.NoButton
                    cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                }
            }

            Text {
                text:
                    "The program is provided AS IS with\n"
                    + "NO WARRANTY OF ANY KIND, INCLUDING\n"
                    + "THE WARRANTY OF DESIGN MERCHANTABILITY,\n"
                    + "AND FITNESS FOR A PARTICULAR PURPOSE."
            }
        }
    }
}
