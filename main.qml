/* ************************************************************************/ /**
 * @file    main.qml
 * *****************************************************************************
 * @brief   Defining main window content...
 *
 * *****************************************************************************
 * *****************************************************************************
 * @author      Alexander Pockes (ATP)   <alexander.pockes@congatec.com>
 *
 * @copyright   Copyright 2016 Alexander Pockes, congatec AG
 *
 * @license     cgtMFGui is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License version 3
 *              as published by the Free Software Foundation.
 *
 *              cgtMFGui is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the
 *              GNU General Public License along with this program.
 *              If not, see <http://www.gnu.org/licenses/>.
 * ************************************************************************/ /**
 **************************************************************************** */


import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls 1.2

import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

import QtQuick.XmlListModel 2.0

import "main.js" as MainJSL

import org.congatec.qt.qml.cgtMFGui 1.0


ApplicationWindow {
    id: mainWin


    //Property declerations
    property int widthVal: 800
    property int heightVal: 320

    property string toolVersion: "v0.3"
    property string qtVersion: "5.7.0"
    property string mingwVersion: "5.3.0 32bit"
    property string copyrightInformation: "2016 Alexander Pockes, congatec AG"

    property string sourceRepoURL: "http://git.congatec.com/imx6-mfg-tool/cgtMFGui"
    property string cgtWebsite: "http://www.congatec.com"
    property string mingwWebsiteURL: "http://mingw.org"
    property string qtWebsiteURL: "http://www.qt.io"

    property string cgtLogoCut: "qrc:/images/cgt_logo_cut.png"
    property string cgtLogoNotext: "qrc:/images/cgt_logo_notext.png"
    property string cgtLogoText: "qrc:/images/cgt_logo_text.png"


    // C++ interaction (cgt c++ qml plugin)
    RunConfig {
        id:runConfig

        // onMemSizeChanged: { console.log("CATCH: signal memSizeChanged (id:runconfig)") }
    }


    // Object properties
    visible: true

    width: widthVal
    height: heightVal
    minimumWidth: widthVal
    minimumHeight: heightVal
    maximumHeight: minimumHeight

    title: qsTr("cgtMFGui")


    // Models
    XmlListModel {
        id: mbtXmlModel
        source: MainJSL.getBasicConfModelSrc()
        query: "/cgtmfgui/mbtype_list/mbtype_item"

        XmlRole { name: "txt"; query: "@txt/string()" }
        XmlRole { name: "val"; query: "@val/string()" }
    }
    XmlListModel {
        id: pnXmlModel
        source: MainJSL.getBasicConfModelSrc()
        query: "/cgtmfgui/pn_list/" + mbtTField.text + "_pn_items/pn_item"

        XmlRole { name: "txt"; query: "@txt/string()" }
        XmlRole { name: "val"; query: "@val/string()" }
        XmlRole { name: "mem_config"; query: "@mem_config/string()" }
    }
    XmlListModel {
        id: actionXmlModel
        source: MainJSL.getBasicConfModelSrc()
        query: "/cgtmfgui/action_list/" + mbtTField.text + "_action_items/action_item"

        XmlRole { name: "txt"; query: "@txt/string()" }
        XmlRole { name: "val"; query: "@val/string()" }
    }
    XmlListModel {
        id: memsizeXmlModel
        source: MainJSL.getBasicConfModelSrc()
        query: "/cgtmfgui/mem_config/size_list/size_item"

        XmlRole { name: "txt"; query: "@txt/string()" }
        XmlRole { name: "val"; query: "@val/string()" }
    }
    XmlListModel {
        id: memwidthXmlModel
        source: MainJSL.getBasicConfModelSrc()
        query: "/cgtmfgui/mem_config/width_list/width_item"

        XmlRole { name: "txt"; query: "@txt/string()" }
        XmlRole { name: "val"; query: "@val/string()" }
    }
    XmlListModel {
        id: memclkXmlModel
        //source: "file:config/basic_conf.xml"
        //source: "file:" + settingsDialog.configFilePath
        source: MainJSL.getBasicConfModelSrc()
        query: "/cgtmfgui/mem_config/clk_list/clk_item"

        XmlRole { name: "txt"; query: "@txt/string()" }
        XmlRole { name: "val"; query: "@val/string()" }
    }


    // Dialogs
    AbDialog {
        id: aboutDialog
    }

    SettDialog {
        id: settingsDialog
    }

    LicDialog {
        id: licensingDialog
    }

    MessageDialog {
        id: settingsVerificationRequiredDialog

        // text: "Will be set externally by caller..."
        // informativeText = "Will be set externally by caller..."

        icon: StandardIcon.Warning
        title: "Settings Verification Required..."

        standardButtons: StandardButton.Ok

        onAccepted: {
            settingsDialog.open();
        }
    }

    // Actions, Menues, Toolbars
    Action {
        id: quitAppAction
        onTriggered: Qt.quit()

        text: qsTr("Quit")
        shortcut: StandardKey.Quit
    }
    Action {
        id: settingsDialogAction
        onTriggered: settingsDialog.open()

        text: qsTr("Settings")
        shortcut: "Alt+S"
    }

    Action {
        id: enableExpertMemConfAction
        checkable: true
        checked: false

        text: qsTr("Enable Guided Memory Configuration Override")
        shortcut: "Alt+G"
    }
    Action {
        id: enableExpertRunConfAction
        checkable: true
        checked: false

        text: qsTr("Enable Manual Run Configuration Override")
        shortcut: "Alt+R"
    }
    Action {
        id: openAboutDialogCgtLogoButtonAction
        onTriggered: aboutDialog.open()
    }
    Action {
        id: licensinInformationDialogAction
        onTriggered: licensingDialog.open()

        text: qsTr("Licensing Information")
    }


    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")

            MenuItem {
                action: settingsDialogAction
            }
            MenuItem {
                action: quitAppAction
            }
        }

        Menu {
            title: qsTr("&Mode")

            MenuItem {
                action: enableExpertMemConfAction
            }
            MenuItem {
                action: enableExpertRunConfAction
            }
        }

        Menu {
            title: qsTr("&Help")

            MenuItem {
                text: "Licensing Information"
                onTriggered: licensingDialog.open()
            }

            MenuItem {
                text: "About"
                onTriggered: aboutDialog.open()
            }
        }
    }

    toolBar: ToolBar {
        id: mainWinToolBar

        RowLayout {
            anchors.fill: parent
            Layout.fillHeight: true

            spacing: 10

            ToolButton {
                id: firstToolButton
                action: enableExpertMemConfAction
                text: enableExpertMemConfAction.checked ? qsTr("Disable Guided Override") : qsTr("Enable Guided Override")
            }
            ToolButton {
                action: enableExpertRunConfAction
                text: enableExpertRunConfAction.checked ? qsTr("Disable Manual Override") : qsTr("Enable Manual Override")
            }

            ToolButton {
                action: settingsDialogAction
            }

            ToolButton {
                action: quitAppAction
                KeyNavigation.tab: mbtCBox
            }
            Item { Layout.fillWidth: true }
            Image {
                source: mainWin.cgtLogoText
                horizontalAlignment: Image.AlignRight
                fillMode: Image.PreserveAspectFit

                Layout.alignment: Qt.AlignRight
                Layout.maximumHeight: firstToolButton.height

                MouseArea {
                    anchors.fill: parent
                    onClicked: aboutDialog.open()
                }
            }
        }
    }


    SplitView{
        anchors.fill: parent

        ScrollView {
            id: pageView
            width: mainWin.widthVal*0.60

            horizontalScrollBarPolicy: Qt.ScrollBarAsNeeded

            Item {
                id: contentLeftSideItem

                width: pageView.viewport.width
                // height: pageView.viewport.height

                Layout.fillWidth: true

                ColumnLayout {
                    id: leftSideColLayout

                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: spacing

                    Layout.fillWidth: true

                    GroupBox {
                        id: basicConfigGBox
                        title: qsTr("Basic Selections")
                        Layout.fillWidth: true

                        GridLayout {
                            anchors.fill: parent
                            columns: 2
                            rows: 3
                            columnSpacing: 25

                            Layout.fillWidth: true

                            Label {
                                text: qsTr("1) Module/Board Type:")
                            }
                            ComboBox {
                                id: mbtCBox
                                KeyNavigation.tab: pnCBox

                                model: mbtXmlModel
                                textRole: "txt"

                                Layout.fillWidth: true

                                onCurrentTextChanged: {
                                    runConfig.moduleBoardType = MainJSL.getModelGenericValue(currentIndex, model, "val");
                                    pnCBox.currentIndex = 0;
                                    actionCBox.currentIndex = 0;
                                }
                            }

                            Label {
                                id: pnLabel
                                text: qsTr("2) Part Number (PN):")
                            }
                            ComboBox {
                                id: pnCBox
                                KeyNavigation.tab: actionCBox

                                model: pnXmlModel
                                textRole: "txt"

                                Layout.fillWidth: true

                                onCurrentTextChanged: {
                                    memsizeCBox.currentIndex = 0;
                                    memwidthCBox.currentIndex = 0;
                                    memclkCBox.currentIndex = 0;

                                    runConfig.partNumber = MainJSL.getModelGenericValue(currentIndex, model, "val");
                                    runConfig.parseXmlBasicConfig(MainJSL.getModelGenericValue(currentIndex, model, "mem_config"), settingsDialog.configFilePath);
                                }

                            }

                            Label {
                                text: qsTr("3) Action:")
                            }
                            ComboBox {
                                id: actionCBox
                                KeyNavigation.tab: gpExConfig.enabled ? memsizeCBox : runButton

                                model: actionXmlModel
                                textRole: "txt"

                                Layout.fillWidth: true

                                //onActivated: console.log(actionXmlModel.get(index).val)
                                onCurrentTextChanged: { runConfig.action = MainJSL.getModelGenericValue(currentIndex, model, "val") }
                            }
                        }
                    }

                    GroupBox {
                        id: gpExConfig

                        title: qsTr("Memory Configuration (Guided Override)")
                        Layout.fillWidth: true

                        enabled: enableExpertMemConfAction.checked == true

                        GridLayout {
                            columns: 2
                            rows: 3
                            columnSpacing: 25

                            anchors.fill: parent
                            Layout.fillWidth: true


                            Label {
                                text: qsTr("Memory Size:")
                            }
                            ComboBox {
                                id: memsizeCBox
                                KeyNavigation.tab: memwidthCBox

                                model: memsizeXmlModel
                                textRole: "txt"

                                Layout.fillWidth: true

                                onCurrentTextChanged: { gpExConfig.enabled && runConfig.setMemSize( MainJSL.getModelGenericValue(currentIndex, model, "val") ) }
                            }

                            Label {
                                text: qsTr("Memory Width:")
                            }
                            ComboBox {
                                id: memwidthCBox
                                KeyNavigation.tab: memclkCBox

                                model: memwidthXmlModel
                                textRole: "txt"

                                Layout.fillWidth: true

                                onCurrentTextChanged: { gpExConfig.enabled && runConfig.setMemWidth( MainJSL.getModelGenericValue(currentIndex, model, "val") ) }
                            }

                            Label {
                                text: qsTr("Memory Clock:")
                            }
                            ComboBox {
                                id: memclkCBox
                                KeyNavigation.tab: currentRunConfigGBox.enabled ? mbtTField : runButton

                                model: memclkXmlModel
                                textRole: "txt"

                                Layout.fillWidth: true

                                onCurrentTextChanged: { gpExConfig.enabled && runConfig.setMemClock( MainJSL.getModelGenericValue(currentIndex, model, "val") ) }
                            }
                        }
                    }
                } // </leftSideColLayout>
            } // </contentLeftSideItem>
        } // </pageView>

        ScrollView {
            id: rightSideView
            width: mainWin.widthVal*0.40

            horizontalScrollBarPolicy: Qt.ScrollBarAsNeeded

            Item {
                width: rightSideView.viewport.width
                // height: rightSideView.viewport.height
                Layout.fillWidth: true

                ColumnLayout {
                    id: rightSideColLayout

                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: spacing

                    Layout.fillWidth: true
                    Layout.leftMargin: 50

                    GroupBox {
                        id: currentRunConfigGBox

                        title: qsTr("Run Configuration (Manual Override)")
                        Layout.fillWidth: true

                        enabled: enableExpertRunConfAction.checked == true

                        GridLayout {
                            columns: 2
                            rows: 6
                            columnSpacing: 25

                            anchors.fill: parent
                            Layout.fillWidth: true

                            Label {
                                text: qsTr("Module/Board Type:")
                            }
                            TextField {
                                id: mbtTField
                                Layout.fillWidth: true
                                //width: 100

                                text: runConfig.moduleBoardType
                                onTextChanged: { runConfig.moduleBoardType = text }
                            }

                            Label {
                                text: qsTr("Part Number (PN):")
                            }
                            TextField {
                                id: pnTField
                                Layout.fillWidth: true

                                text: runConfig.partNumber
                                onTextChanged: { runConfig.partNumber = text }
                            }

                            Label {
                                text: qsTr("Action:")
                            }
                            TextField {
                                id: actionTField
                                Layout.fillWidth: true

                                text: runConfig.action
                                onTextChanged: { runConfig.action = text }
                            }

                            Label {
                                text: qsTr("Memory Size:")
                            }
                            TextField {
                                id: memSizeTField
                                Layout.fillWidth: true

                                text: runConfig.memSize
                                onTextChanged: { runConfig.memSize = text }
                            }

                            Label {
                                text: qsTr("Memory Width:")
                            }
                            TextField {
                                id: memWidthTField
                                Layout.fillWidth: true

                                text: runConfig.memWidth
                                onTextChanged: { runConfig.memWidth = text }
                            }

                            Label {
                                text: qsTr("Memory Clock:")
                            }
                            TextField {
                                id: memClockTField
                                Layout.fillWidth: true
                                KeyNavigation.tab: runButton

                                text: runConfig.memClock
                                onTextChanged: { runConfig.memClock = text }
                            }
                        }
                    } // </currentRunConfigGBox>
                    Button {
                        id: runButton
                        text: qsTr("Run MFGTool2")
                        Layout.fillWidth: true

                        onClicked: {
                            // XXX debug...
                            runConfig.printRunConfig();

                            // (1) check preconditions
                            if( ! runConfig.fileExists( settingsDialog.configFilePath ) )
                            {
                                settingsVerificationRequiredDialog.text = "Path to cgtMFGui Config File does NOT exist!"
                                settingsVerificationRequiredDialog.informativeText = "Please set a proper path to the cgtMFGui Config File at the Settings dialog"
                                settingsVerificationRequiredDialog.open();
                                return;
                            }

                            if( ! runConfig.setExecutablePath(settingsDialog.execPath) )
                            {
                                settingsVerificationRequiredDialog.text = "Path to MFGTool2 executable does NOT exist!"
                                settingsVerificationRequiredDialog.informativeText = "Please set a proper path to the MFGTool2 executable at the Settings dialog"
                                settingsVerificationRequiredDialog.open();
                                return;
                            }

                            // (2) prepare MFGTool2 execution
                            runConfig.setProcessArguments();

                            // (3) run MFGTool2
                            runConfig.runProcess();
                        }
                    }
                }
            }
        } // </rightSideView>
    } // SplitView
} // </mainWin>
