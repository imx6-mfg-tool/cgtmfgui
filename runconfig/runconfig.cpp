/* ************************************************************************/ /**
 * @file    runconfig.cpp
 * *****************************************************************************
 * @brief   QML C++ extension plugin providing easy data exchange between C++
 *          and QML code and vice versa.
 *          This modules abstracts all methods and data required to build
 *          up a MFGTool2 compatible commandline arguments based on the
 *          user's choices.
 *
 * *****************************************************************************
 * *****************************************************************************
 * @author      Alexander Pockes (ATP)   <alexander.pockes@congatec.com>
 *
 * @copyright   Copyright 2016 Alexander Pockes, congatec AG
 *
 * @license     cgtMFGui is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License version 3
 *              as published by the Free Software Foundation.
 *
 *              cgtMFGui is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the
 *              GNU General Public License along with this program.
 *              If not, see <http://www.gnu.org/licenses/>.
 * ************************************************************************/ /**
 **************************************************************************** */


#include "runconfig.h"
#include <QDir>
#include <QDebug>


// ->
// constructor and initial setup definitions
// -->

/* ************************************************************************/ /**
 * @fn RunConfig(QObject *parent)
 * *****************************************************************************
 *
 * @brief       standard constructor
 *
 * @param       parent          Qt derivation hierarchy...
 *
 **************************************************************************** */
RunConfig::RunConfig(QObject *parent) : QQmlExtensionPlugin(parent)
{
}

/* ************************************************************************/ /**
 * @fn registerTypes(const char *uri)
 * *****************************************************************************
 *
 * @brief       registering the class (C++ type) in the QML system (QML plugin)
 *
 * @param       uri             distinct identifier; this identifier is used
 *                              to import the extension plugin from QML
 *
 **************************************************************************** */
void RunConfig::registerTypes(const char *uri)
{
    //Q_ASSERT(uri == QLatin1String("org.congatec.qt.qml"));
    qmlRegisterType<RunConfig>(uri, 1, 0, "RunConfig");
}


// ->
// setter definitions
// -->

/* ************************************************************************/ /**
 * @fn setModuleBoardType(QString mbt)
 * *****************************************************************************
 *
 * @brief       simple setter method...
 *              -> emitting *Changed() signal, required by QML property binding
 *              -> setting appropriate QML property calls this method
 *              -> invokable from QML
 *
 * @param       mbt            module board type
 *
 **************************************************************************** */
void RunConfig::setModuleBoardType(QString mbt)
{
    if ( mbt != this->m_moduleBoardType )
    {
        this->m_moduleBoardType = mbt;
        emit moduleBoardTypeChanged();
    }
}

/* ************************************************************************/ /**
 * @fn setPartNumber(QString pn)
 * *****************************************************************************
 *
 * @brief       simple setter method...
 *              -> emitting *Changed() signal, required by QML property binding
 *              -> setting appropriate QML property calls this method
 *              -> invokable from QML
 *
 * @param       pn         part number
 *
 **************************************************************************** */
void RunConfig::setPartNumber(QString pn)
{
    if ( pn != this->m_partNumber )
    {
        this->m_partNumber = pn;
        emit partNumberChanged();
    }
}

/* ************************************************************************/ /**
 * @fn setAction(QString act)
 * *****************************************************************************
 *
 * @brief       simple setter method...
 *              -> emitting *Changed() signal, required by QML property binding
 *              -> setting appropriate QML property calls this method
 *              -> invokable from QML
 *
 * @param       act          action to perform
 *
 **************************************************************************** */
void RunConfig::setAction(QString act)
{
    if ( act != this->m_action )
    {
        this->m_action = act;
        emit actionChanged();
    }
}

/* ************************************************************************/ /**
 * @fn setMemSize(QString size)
 * *****************************************************************************
 *
 * @brief       simple setter method...
 *              -> emitting *Changed() signal, required by QML property binding
 *              -> setting appropriate QML property calls this method
 *              -> invokable from QML
 *
 * @param       size            memory size in MB
 *
 **************************************************************************** */
void RunConfig::setMemSize(QString size)
{

    if ( size != this->m_memSize )
    {
        this->m_memSize = size;
        qDebug() << "INFO: memSize changed -> memSize = " << m_memSize;

        qDebug() << "EMIT: signal memSizeChanged";
        emit memSizeChanged();
    }
}

/* ************************************************************************/ /**
 * @fn setMemWidth(QString width)
 * *****************************************************************************
 *
 * @brief       simple setter method...
 *              -> emitting *Changed() signal, required by QML property binding
 *              -> setting appropriate QML property calls this method
 *              -> invokable from QML
 *
 * @param       width           memory width in Bit
 *
 **************************************************************************** */
void RunConfig::setMemWidth(QString width)
{
    if ( width != this->m_memWidth )
    {
        this->m_memWidth = width;
        emit memWidthChanged();
    }
}

/* ************************************************************************/ /**
 * @fn setMemClock(QString clk)
 * *****************************************************************************
 *
 * @brief       simple setter method...
 *              -> emitting *Changed() signal, required by QML property binding
 *              -> setting appropriate QML property calls this method
 *              -> invokable from QML
 *
 * @param       clk            memory clock in Hz
 *
 **************************************************************************** */
void RunConfig::setMemClock(QString clk)
{
    if ( clk != this->m_memClock )
    {
        this->m_memClock = clk;
        emit memClockChanged();
    }
}


// ->
// getter definitions
// -->

/* ************************************************************************/ /**
 * @fn getModuleBoardType()
 * *****************************************************************************
 *
 * @brief       simple getter method...
 *              -> evaluating appropriate QML property calls this method
 *              -> invokable from QML
 *
 **************************************************************************** */
QString RunConfig::getModuleBoardType()
{
    return this->m_moduleBoardType;
}

/* ************************************************************************/ /**
 * @fn getPartNumber()
 * *****************************************************************************
 *
 * @brief       simple getter method...
 *              -> evaluating appropriate QML property calls this method
 *              -> invokable from QML
 *
 **************************************************************************** */
QString RunConfig::getPartNumber()
{
    return this->m_partNumber;
}

/* ************************************************************************/ /**
 * @fn getAction()
 * *****************************************************************************
 *
 * @brief       simple getter method...
 *              -> evaluating appropriate QML property calls this method
 *              -> invokable from QML
 *
 **************************************************************************** */
QString RunConfig::getAction()
{
    return this->m_action;
}

/* ************************************************************************/ /**
 * @fn getMemSize()
 * *****************************************************************************
 *
 * @brief       simple getter method...
 *              -> evaluating appropriate QML property calls this method
 *              -> invokable from QML
 *
 **************************************************************************** */
QString RunConfig::getMemSize()
{
    return this->m_memSize;
}

/* ************************************************************************/ /**
 * @fn getMemWidth()
 * *****************************************************************************
 *
 * @brief       simple getter method...
 *              -> evaluating appropriate QML property calls this method
 *              -> invokable from QML
 *
 **************************************************************************** */
QString RunConfig::getMemWidth()
{
    return this->m_memWidth;
}

/* ************************************************************************/ /**
 * @fn getMemClock()
 * *****************************************************************************
 *
 * @brief       simple getter method...
 *              -> evaluating appropriate QML property calls this method
 *              -> invokable from QML
 *
 **************************************************************************** */
QString RunConfig::getMemClock()
{
    return this->m_memClock;
}


// ->
// xml config file related
// -->

/* ************************************************************************/ /**
 * @fn findXmlConfigItemByAttr( QXmlStreamReader &currentNode,
 *                              QString nodeName,
 *                              QString attrName,
 *                              QString attrVal )
 * *****************************************************************************
 *
 * @brief       looking for a node identified by a nodename + particular
 *              attrName/attrVal combination
 *
 * @param       currentNode     xml node object to start search for a node
 *
 *              nodeName        looking for node "nodeName"
 *
 *              attrName        looking for node "nodeName" with attr
 *                              "attrName"
 *
 *              attrVal         loking for node "nodeName" with attr
 *                              "attrName" and attr value "attrValue"
 *
 * @return      true            node found
 *              false           node not found
 *
 **************************************************************************** */
bool RunConfig::findXmlConfigItemByAttr(QXmlStreamReader &currentNode, QString nodeName, QString attrName, QString attrVal)
{
    int i = 0;

    if ( !currentNode.isStartElement() )
    {
        return false;
    }

    if ( currentNode.name().toString() == nodeName )
    {
        // qDebug() << "Item Node found -> " << currentNode.name().toString();
        if ( currentNode.attributes().size() > 0 )
        {
            for ( i=0; i<currentNode.attributes().size(); i++ )
            {
                // qDebug() << "attr found";
                if ( currentNode.attributes().at(i).name() == attrName )
                {
                    if ( currentNode.attributes().at(i).value() == attrVal )
                    {
                        qDebug() << "Node     : " << currentNode.name().toString() << endl \
                                 << "AttrName : " << currentNode.attributes().at(0).name() << endl \
                                 << "AttrValue: " << currentNode.attributes().at(0).value();

                        return true;
                    }
                }
            }
            return false;
        }
    }
    return false;
}

/* ************************************************************************/ /**
 * @fn getXmlConfigAttrValue( QXmlStreamReader &currentNode,
 *                            QString nodeName,
 *                            QString attrName )
 * *****************************************************************************
 *
 * @brief       extracting attr value from the currentNode's attr identified
 *              by attrName; the nodeName is just given to ensure, that
 *              "currentNode" references the expected node
 *
 * @param       currentNode     xml node from which the attr value has to be
 *                              extracted
 *
 *              nodeName        expected node name
 *
 *              attrName        identify the attr from which the attr value has
 *                              to be extracted
 *
 * @return      currentNode
 *              is no start
 *              element         ""
 *
 *              currentNode's
 *              name
 *              is != nodeName  ""
 *
 *              attr not found  ""
 *
 *              SUCCESS:        attrName's attr value
 *
 *  XXX:
 *              to many cases return "" ... no posibility to distinguish....
 *
 **************************************************************************** */
QString RunConfig::getXmlConfigAttrValue(QXmlStreamReader &currentNode, QString nodeName, QString attrName)
{
    int i = 0;

    if ( !currentNode.isStartElement() )
        return "";


    if ( currentNode.name().toString() != nodeName )
        return "";

    for ( i = 0; i < currentNode.attributes().size(); i++ )
    {
        if ( currentNode.attributes().at(i).name() == attrName )
        {
            return currentNode.attributes().at(i).value().toString();
        }
    }
    return "";
}

/* ************************************************************************/ /**
 * @fn parseXmlBasicConfig(QString id, QString configFilePath)
 * *****************************************************************************
 *
 * @brief       selection and extracting the proper memory configuration
 *              on the basis of a given identifier (part number)
 *
 * @param       id              identifier (part number)
 *
 *              configFilePath  path to the xml config file to parse
 *
 **************************************************************************** */
void RunConfig::parseXmlBasicConfig(QString id, QString configFilePath)
{
    bool retval = false;

    QString tmpAttrName = "";
    QString tmpAttrVal = "";

    QString XmlBasicConfigFileName = configFilePath;
    QFile XmlBasicConfigFile(XmlBasicConfigFileName);


    if( !XmlBasicConfigFile.open(QFile::ReadOnly | QFile::Text) )
    {
        qCritical() << "ERROR: loading xml file \"" << XmlBasicConfigFileName << "\" failed" << endl;
    }
    this->XmlBasicConfig.setDevice(&XmlBasicConfigFile);


    while( !XmlBasicConfig.atEnd() )
    {
        retval = findXmlConfigItemByAttr(XmlBasicConfig, "config_item", "id", id);
        if ( retval == true )
        {
            // XXX: getXmlConfigAttrValue returns "", if the given attr isn't found. Issue?
            setMemSize( getXmlConfigAttrValue(XmlBasicConfig, "config_item", "size") );
            setMemWidth( getXmlConfigAttrValue(XmlBasicConfig, "config_item", "width") );
            setMemClock( getXmlConfigAttrValue(XmlBasicConfig, "config_item", "clk") );
            break;
        }
        XmlBasicConfig.readNext();
    }

    XmlBasicConfigFile.close();
}


// ->
// executing third party program
// -->

/* ************************************************************************/ /**
 * @fn printRunConfig()
 * *****************************************************************************
 *
 * @brief       simply printing the essential runConfig attributes...
 *              -> debugging purposes...
 *
 **************************************************************************** */
void RunConfig::printRunConfig()
{
    qDebug() << this->m_moduleBoardType << endl;
    qDebug() << this->m_partNumber << endl;
    qDebug() << this->m_action << endl;

    qDebug() << this->m_memSize << endl;
    qDebug() << this->m_memWidth << endl;
    qDebug() << this->m_memClock << endl;
}

/* ************************************************************************/ /**
 * @fn setExecutablePath(QString execPath)
 * *****************************************************************************
 *
 * @brief       simple setter method...
 *              -> Invokable from QML
 *              -> does NOT emit a signal
 *                  => execPath does NOT support QML property binding (no need)
 *
 * @param       execPath        path to the 3rd party application (MFGTool2)
 *                              which will be executed by cgtMFGui
 *
 **************************************************************************** */
bool RunConfig::setExecutablePath(QString execPath)
{
    // XXX
    // - bisher wird immer gesetzt -> signal slot schleife verurusachen...
    if( ! QFile::exists(execPath) )
    {
        qDebug("INFO: executablePath does NOT exist...");
        return false;
    }

    this->executablePath = execPath;
    qDebug() << "INFO: executablePath = " << this->executablePath << endl;
    return true;
}

/* ************************************************************************/ /**
 * @fn setProcessArguments()
 * *****************************************************************************
 *
 * @brief       building the commandline arguments from the runconfig's member
 *              attributes for execution of the 3rd party application (MFGTool2)
 *              -> Invokable from QML
 *
 **************************************************************************** */
void RunConfig::setProcessArguments()
{
    // XXX: Qt way of life does not work -> due to MFTool's  confusing cmdl handling...
    /*
    this->processArguments.clear();
    this->processArguments << QString("-c " + this->moduleBoardType);
                           << QString("-l") << QString("\"") + this->action + QString("\"") \
                           << QString("-s") << QString("\"") + QString("PN=") + this->partNumber + QString("\"") \
                           << QString("-s") << QString("\"") + QString("MEM_SIZE=")  + this->memSize  + QString("\"") \
                           << QString("-s") << QString("\"") + QString("MEM_WIDTH=") + this->memWidth + QString("\"") \
                           << QString("-s") << QString("\"") + QString("MEM_CLK=")   + this->memClock + QString("\"");

    qDebug() << "arguments: " << endl << this->processArguments << endl;
    */

    /*
    this->pArgs = QString(\
                "-c " + QString("\"") + this->moduleBoardType + '"' + " " + \
                "-l " + '"' + this->action + '"' + " " \
                "-s " + '"' + "PN=" + this->partNumber + '"' + " " + \
                "-s " + '"' + "MEM_SIZE="  + this->memSize  + '"' + " " + \
                "-s " + '"' + "MEM_WIDTH=" + this->memWidth + '"' + " " + \
                "-s " + '"' + "MEM_CLK="   + this->memClock + '"' \
                );
    */
    this->pArgs = QString(\
                "-c " + QString("\"") + this->m_moduleBoardType + "\"" + " " + \
                "-l " + "\"" + this->m_action + "\"" + " " \
                "-s " + "\"" + "PN=" + this->m_partNumber +"\"" + " " + \
                "-s " + "\"" + "MEM_SIZE="  + this->m_memSize  + "\"" + " " + \
                "-s " + "\"" + "MEM_WIDTH=" + this->m_memWidth + "\"" + " " + \
                "-s " + "\"" + "MEM_CLK="   + this->m_memClock + "\"" \
                );

    qDebug() << "process arguments: " << endl << this->pArgs << endl;
}

/* ************************************************************************/ /**
 * @fn runProcess()
 * *****************************************************************************
 *
 * @brief   executing the 3rd party application (MFGTool2)
 *          -> Invokable from QML
 *
 **************************************************************************** */
void RunConfig::runProcess()
{
    qDebug() << "<runProcess> triggered!" << endl;

    // XXX: Qt way of life does not work -> due to MFGTool's confusing cmdl handling...
    /*
    this->process.setProgram(this->executablePath);
    this->process.setArguments(this->processArguments);
    this->process.start();
    */

    // XXX: dirty workaround...
    this->process.setProgram(QString(this->executablePath) + QString(" ") + QString(this->pArgs));
    this->process.start();
}


// ->
// generic purpose
// -->

/* ************************************************************************/ /**
 * @fn fileExists( QString filePath )
 * *****************************************************************************
 *
 * @brief       checks if the given filePath exists
 *
 * @return      true    file exists
 *              false   file does not exist
 *
 **************************************************************************** */
bool RunConfig::fileExists( QString filePath )
{
    return QFile::exists(filePath);
}
