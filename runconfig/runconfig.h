/* ************************************************************************/ /**
 * @file    runconfig.h
 * *****************************************************************************
 * @brief   QML C++ extension plugin providing easy data exchange between C++
 *          and QML code and vice versa.
 *          This modules abstracts all methods and data required to build
 *          up a MFGTool2 compatible commandline arguments based on the
 *          user's choices.
 *
 * *****************************************************************************
 * *****************************************************************************
 * @author      Alexander Pockes (ATP)   <alexander.pockes@congatec.com>
 *
 * @copyright   Copyright 2016 Alexander Pockes, congatec AG
 *
 * @license     cgtMFGui is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License version 3
 *              as published by the Free Software Foundation.
 *
 *              cgtMFGui is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the
 *              GNU General Public License along with this program.
 *              If not, see <http://www.gnu.org/licenses/>.
 * ************************************************************************/ /**
 **************************************************************************** */


#ifndef RUNCONFIG_H
#define RUNCONFIG_H

#include <QObject>
#include <QString>
#include <QQmlExtensionPlugin>
#include <QQmlEngine>

#include <QProcess>
#include <QStringList>

#include <QFile>
#include <QXmlStreamReader>


/* ************************************************************************/ /**
 * @class RunConfig : public QQmlExtensionPlugin
 * *****************************************************************************
 *
 * @brief       This class is registered as QML type, so it is also accessible
 *              from QML. It's main purpose is to simplify data exchange
 *              between C++ and QML code, as well as to simplify triggering
 *              triggering C++ actions from QML side.
 *
 *              This class abstracts all properties and actions required
 *              to specify and execute a third party application (MFGTool2).
 *
 **************************************************************************** */
class RunConfig : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.congatec.qt.qml.cgtMFGui")

    // property definitions: mapping private members, setters and getters to QML property mechanism
    Q_PROPERTY(QString moduleBoardType READ getModuleBoardType WRITE setModuleBoardType NOTIFY moduleBoardTypeChanged)
    Q_PROPERTY(QString partNumber READ getPartNumber WRITE setPartNumber NOTIFY partNumberChanged)
    Q_PROPERTY(QString action READ getAction WRITE setAction NOTIFY actionChanged)

    Q_PROPERTY(QString memSize READ getMemSize WRITE setMemSize NOTIFY memSizeChanged)
    Q_PROPERTY(QString memWidth READ getMemWidth WRITE setMemWidth NOTIFY memWidthChanged)
    Q_PROPERTY(QString memClock READ getMemClock WRITE setMemClock NOTIFY memClockChanged)


private:
    QString m_moduleBoardType;  ///< module board type id ("val"), passed to MFGTool2
    QString m_partNumber;       ///< part number id ("val"), passed to MFGTool2
    QString m_action;           ///< action id ("val"), passed to MFGTool2

    QString m_memSize;          ///< memory size id ("val"), passed to MFGTool2
    QString m_memWidth;         ///< memory width id ("val"), passed to MFGTool2
    QString m_memClock;         ///< memory clock ("val"), passed to MFGTool2

    QProcess process;           ///< process object to start 3rd party application
    QString executablePath;     ///< path to 3rd party application
    QString pArgs;              ///< commandline arguments, passed to QProcess process

    // XXX: Qt way of life (QProcess) parameter handling does not work with MFGTool's confusing parameter handling...
    // QStringList processArguments;

    QString xmlBasicConfigFilePath;     ///< path to basic_conf.xml
    QXmlStreamReader XmlBasicConfig;    ///< action id ("val"), passed to MFGTool2


public:
    explicit RunConfig(QObject *parent = 0);
    void registerTypes(const char *uri);

    // setters (req by property binding mechanism, emitting *Changed signals)
    Q_INVOKABLE void setModuleBoardType(QString mbt);
    Q_INVOKABLE void setPartNumber(QString pn);
    Q_INVOKABLE void setAction(QString act);

    Q_INVOKABLE void setMemSize(QString size);
    Q_INVOKABLE void setMemWidth(QString width);
    Q_INVOKABLE void setMemClock(QString clk);

    // normal setters
    Q_INVOKABLE bool setExecutablePath(QString execPath);

    // getters
    Q_INVOKABLE QString getModuleBoardType();
    Q_INVOKABLE QString getPartNumber();
    Q_INVOKABLE QString getAction();

    Q_INVOKABLE QString getMemSize();
    Q_INVOKABLE QString getMemWidth();
    Q_INVOKABLE QString getMemClock();


    // executing 3rd party program
    Q_INVOKABLE void printRunConfig();
    Q_INVOKABLE void setProcessArguments();
    Q_INVOKABLE void runProcess();


    // passing/extracting infos from xml config file
    Q_INVOKABLE void parseXmlBasicConfig(QString, QString);
    // Q_INVOKABLE void parseXmlBasicConfig(QString);
    QString getXmlConfigAttrValue(QXmlStreamReader &currentNode, QString nodeName, QString attrName);
    bool findXmlConfigItemByAttr(QXmlStreamReader &currentNode, QString, QString, QString);

    // general purpose
    Q_INVOKABLE bool fileExists(QString filePath);


    // *Changed signals req by property binding mechanism
signals:
    void moduleBoardTypeChanged(void);
    void partNumberChanged(void);
    void actionChanged(void);

    void memSizeChanged(void);
    void memWidthChanged(void);
    void memClockChanged(void);


public slots:


};

#endif // RUNCONFIG_H
